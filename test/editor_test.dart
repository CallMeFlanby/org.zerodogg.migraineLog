// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2024    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:MigraineLog/editor.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:MigraineLog/genericwidgets.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mocktail/mocktail.dart';
import 'package:platform/platform.dart';
import 'utils.dart';
import 'fakes.dart';

void main() {
  var skipGoldenTests = !LocalPlatform().isLinux;

  setUpAll(() async {
    registerFallbackValue(FakeRoute());
    registerFallbackValue(FakeMigraineEntry());
    await initializeDateFormatting("en", null);
  });

  setTestFile('editor');

  group('MigraineNoteEditor', () {
    testWidgets('golden', (WidgetTester tester) async {
      await goldenTest(
        widgetType: MigraineNoteEditor,
        widgetInstance: MigraineNoteEditor(entry: MigraineEntry()),
        tester: tester,
      );
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      await tester.pumpWidget(materialWidget(MigraineNoteEditor(entry: e)));
      await tester.enterText(find.byType(TextField), 'Hello World');
      expect(e.note, "Hello World");
    });
  });
  group('MigraineMedsTakenSelector', () {
    var dummyMeds = ['Test1', 'Test2'];
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      e.medications.addList(dummyMeds);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineMedsTakenSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: MigraineLogConfig()),
        ],
      ));
      await goldenTest(
        widgetType: MigraineMedsTakenSelector,
        tester: tester,
        name: "MigraineMedsTakenSelector",
      );
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.medications.addList(dummyMeds);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineMedsTakenSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(e.medications.hasEntryNamed(dummyMeds[0]), isFalse);
      // All config meds should be displayed
      for (var dummy in dummyMeds) {
        expect(find.text(dummy), findsOneWidget);
      }
      // Tapping a med should work
      await tester.tap(find.text(dummyMeds[0]));
      await tester.pump();
      expect(e.medications.hasEntryNamed(dummyMeds[0]), isTrue);
      // Tapping the second one should work
      expect(e.medications.hasEntryNamed(dummyMeds[1]), isFalse);
      await tester.tap(find.text(dummyMeds[1]));
      await tester.pump();
      expect(e.medications.hasEntryNamed(dummyMeds[1]), isTrue);
      await tester.flush();
    });
  });
  group('MigraineMedsTakenList', () {
    var dummyMeds = [
      MigraineMedicationEntry(medication: "Test med 1", hour: 10, minute: 25),
      MigraineMedicationEntry(medication: "Test med 2", hour: 22, minute: 35)
    ];
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      e.medications.addList(dummyMeds);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineMedsTakenList(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: MigraineLogConfig()),
        ],
      ));
      await goldenTest(
        widgetType: MigraineMedsTakenList,
        tester: tester,
        name: "MigraineMedsTakenList",
      );
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      e.medications.addList(dummyMeds);
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineMedsTakenList(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      // All config meds should be displayed
      for (var dummy in dummyMeds) {
        expect(find.text(dummy.medication), findsOneWidget);
      }
      expect(e.medications.has(dummyMeds[0]), isTrue);
      // Swiping away should delete it
      await tester.drag(find.byType(Icon).first, const Offset(500, 0));
      await tester.pumpAndSettle();
      expect(e.medications.has(dummyMeds[0]), isFalse);

      expect(dummyMeds[1].hour, 22);
      expect(dummyMeds[1].minute, 35);
      // Tapping should open up the time selector
      await tester.tap(find.byType(Icon).first);
      await tester.pumpAndSettle();

      await tester.tap(find.byTooltip('Switch to text input mode'));
      await tester.pumpAndSettle();

      // Enter new time
      final inputs = find.byType(TextFormField);

      await tester.enterText(inputs.at(0), "12");
      await tester.enterText(inputs.at(1), "13");

      await tester.tap(find.text('OK'));
      await tester.pumpAndSettle();

      expect(dummyMeds[1].hour, 12);
      expect(dummyMeds[1].minute, 13);

      await tester.flush();
    });
  });
  group('MigraineStrengthSelector', () {
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineStrengthSelector,
        tester: tester,
        name: "MigraineStrengthSelector",
      );
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      Finder selected =
          find.byWidgetPredicate((w) => w is RadioListTile && w.checked);
      expect(selected, findsNothing);
      expect(e.strength, isNull);

      await tester.tap(find.text(c.messages.strongMigraineMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.strongMigraine);

      await tester.tap(find.text(c.messages.migraineMesssage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.migraine);

      await tester.tap(find.text(c.messages.headacheMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.headache);

      expect(c.enableAuraStrength, false);
      expect(find.text(c.messages.auraOnlyMessage()), findsNothing,
          reason: 'Aura only should not be displayed unless enabled');

      expect(c.enableNoHeadacheStrength, false);
      expect(find.text(c.messages.noHeadacheMessage()), findsNothing,
          reason: 'NoHeadache only should not be displayed unless enabled');

      await tester.flush();
    });
    testWidgets('Aura strength without feature enabled',
        (WidgetTester tester) async {
      var e = MigraineEntry();
      e.strength = MigraineStrength.auraOnly;
      var c = MigraineLogConfig();
      // Being explicit despite the default
      c.enableAuraStrength = false;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.text(c.messages.auraOnlyMessage()), findsOneWidget,
          reason:
              'Aura only should be displayed despite setting if that is selected');

      await tester.flush();
    });
    testWidgets('NoHeadache strength without feature enabled',
        (WidgetTester tester) async {
      var e = MigraineEntry();
      e.strength = MigraineStrength.noHeadache;
      var c = MigraineLogConfig();
      // Being explicit despite the default
      c.enableNoHeadacheStrength = false;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.text(c.messages.noHeadacheMessage()), findsOneWidget,
          reason:
              'NoHeadache only should be displayed despite setting if that is selected');

      await tester.flush();
    });
    testWidgets('Help golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      await goldenTest(
        widgetType: AlertDialog,
        tester: tester,
        name: "MigraineStrengthSelector.help",
      );
      await tester.flush();
    }, skip: skipGoldenTests);
    testWidgets('Help functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      expect(find.byType(AlertDialog), findsOneWidget);
      expect(find.text('CLOSE'), findsOneWidget);
      await tester.tap(find.text('CLOSE'));
      await tester.pump();
      expect(find.byType(AlertDialog), findsNothing);
      await tester.flush();
    });
  });
  group('MigraineStrengthSelector with aura', () {
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableAuraStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineStrengthSelector,
        tester: tester,
        name: "MigraineStrengthSelectorWithAura",
      );
      await tester.flush();
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableAuraStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      Finder selected =
          find.byWidgetPredicate((w) => w is RadioListTile && w.checked);
      expect(selected, findsNothing);
      expect(e.strength, isNull);

      await tester.tap(find.text(c.messages.strongMigraineMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.strongMigraine);

      await tester.tap(find.text(c.messages.migraineMesssage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.migraine);

      await tester.tap(find.text(c.messages.headacheMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.headache);

      await tester.tap(find.text(c.messages.auraOnlyMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.auraOnly);

      await tester.flush();
    });
    testWidgets('Help golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableAuraStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      await goldenTest(
        widgetType: AlertDialog,
        tester: tester,
        name: "MigraineStrengthSelectorWithAura.help",
      );
      await tester.flush();
    }, skip: skipGoldenTests);
    testWidgets('Help functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableAuraStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      expect(find.byType(AlertDialog), findsOneWidget);
      expect(find.text('CLOSE'), findsOneWidget);
      await tester.tap(find.text('CLOSE'));
      await tester.pump();
      expect(find.byType(AlertDialog), findsNothing);
      await tester.flush();
    });
  });
  group('MigraineStrengthSelector with NoHeadache', () {
    testWidgets('golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableNoHeadacheStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineStrengthSelector,
        tester: tester,
        name: "MigraineStrengthSelectorWithNoHeadache",
      );
      await tester.flush();
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableNoHeadacheStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      Finder selected =
          find.byWidgetPredicate((w) => w is RadioListTile && w.checked);
      expect(selected, findsNothing);
      expect(e.strength, isNull);

      await tester.tap(find.text(c.messages.strongMigraineMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.strongMigraine);

      await tester.tap(find.text(c.messages.migraineMesssage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.migraine);

      await tester.tap(find.text(c.messages.headacheMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.headache);

      await tester.tap(find.text(c.messages.noHeadacheMessage()));
      await tester.pump();
      expect(selected, findsOneWidget);
      expect(e.strength, MigraineStrength.noHeadache);

      await tester.flush();
    });
    testWidgets('Help golden', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableNoHeadacheStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      await goldenTest(
        widgetType: AlertDialog,
        tester: tester,
        name: "MigraineStrengthSelectorWithNoHeadache.help",
      );
      await tester.flush();
    }, skip: skipGoldenTests);
    testWidgets('Help functionality', (WidgetTester tester) async {
      var e = MigraineEntry();
      var c = MigraineLogConfig();
      c.enableNoHeadacheStrength = true;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineStrengthSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byType(AlertDialog), findsNothing);
      expect(find.byIcon(Icons.help), findsOneWidget);
      await tester.tap(find.byIcon(Icons.help));
      await tester.pump();
      expect(find.byType(AlertDialog), findsOneWidget);
      expect(find.text('CLOSE'), findsOneWidget);
      await tester.tap(find.text('CLOSE'));
      await tester.pump();
      expect(find.byType(AlertDialog), findsNothing);
      await tester.flush();
    });
  });
  group('MigraineDateSelector', () {
    testWidgets('golden', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 2, 19);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await goldenTest(
        widgetType: MigraineDateSelector,
        tester: tester,
        name: "MigraineDateSelector",
      );
    }, skip: skipGoldenTests);
    testWidgets('Functionality', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 1, 1);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await tester.pump();
      await tester.tap(find.byWidgetPredicate((w) => w is ElevatedButton));
      await tester.pump();
      expect(find.byWidgetPredicate((w) => w is CalendarDatePicker),
          findsOneWidget);
      await tester.tap(find.text("2"));
      await tester.pump();
      await tester.tap(find.text("OK"));
      await tester.pump();
      expect(dateString(e.date), dateString(DateTime(2021, 1, 2)));
    });
    testWidgets('Edit warning', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 1, 1);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(editMode: DateTime.now()),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await tester.pump();
      var warning = RegExp(r"You're editing");
      expect(
          find.byWidgetPredicate(
              (w) => w is MDTextBox && warning.hasMatch(w.text!)),
          findsOneWidget);
    });
    testWidgets('Overwrite warning', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var l = MigraineList(config: MigraineLogConfig());
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 1, 1);
      var e2 = MigraineEntry(parentList: l);
      e2.date = DateTime(2021, 1, 1);
      l.set(e2);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineDateSelector(),
        provide: [
          ChangeNotifierProvider.value(value: e),
        ],
      ));
      await tester.pump();
      var warning = RegExp(r"Warning");
      expect(
          find.byWidgetPredicate(
              (w) => w is MDTextBox && warning.hasMatch(w.text!)),
          findsOneWidget);

      await tester.flush();
    });
  });
  group('MigraineLogEditor', () {
    testWidgets('golden', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var e = MigraineEntry(parentList: l);
      e.date = DateTime(2021, 2, 19);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogEditor(entry: e),
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogEditor,
        tester: tester,
        name: "MigraineLogEditor",
      );
    }, skip: skipGoldenTests);
    testWidgets('Saving without strength', (WidgetTester tester) async {
      var navO = MockNavigatorObserver();
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MockMigraineList();
      when(() => l.exists(any())).thenReturn(false);
      var e = MigraineEntry(parentList: l);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogEditor(entry: e),
        navObserver: navO,
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsNothing);
      await tester.tap(find.byIcon(Icons.save));
      await tester.pump();
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsOneWidget,
          reason: 'SnackBar with error message should be displayed');
      verifyNever(() => navO.didPop(any(), any()));
      verifyNever(() => l.set(any()));
    });
    testWidgets('Saving', (WidgetTester tester) async {
      var navO = MockNavigatorObserver();
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MockMigraineList();
      when(() => l.exists(any())).thenReturn(false);
      var e = MigraineEntry(parentList: l);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogEditor(entry: e),
        navObserver: navO,
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await tester.tap(find.text(c.messages.strongMigraineMessage()));
      expect(find.byWidgetPredicate((w) => w is SnackBar), findsNothing);
      await tester.tap(find.byIcon(Icons.save));
      await tester.pump();
      verify(() => navO.didPop(any(), any()));

      verify(() => l.set(e));

      await tester.flush();
    });
  });
}

class MockMigraineList extends Mock implements MigraineList {}
