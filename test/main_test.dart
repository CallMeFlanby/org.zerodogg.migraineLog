// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2025   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// ignore_for_file: prefer_function_declarations_over_variables

import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:MigraineLog/main.dart';
import 'package:MigraineLog/viewer.dart';
import 'package:MigraineLog/stats.dart';
import 'package:provider/provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'dart:io';
import 'package:mocktail/mocktail.dart';
import 'package:flutter/material.dart';
import 'package:platform/platform.dart';
import 'utils.dart';
import 'fakes.dart';
import 'main_mocks.dart';

void main() {
  var skipGoldenTests = !LocalPlatform().isLinux;

  setUpAll(() {
    registerFallbackValue(FakeRoute());
  });

  setTestFile('main');
  // Needed for the getApplicationDocumentsDirectory mock to work.
  TestWidgetsFlutterBinding.ensureInitialized();
  packageInfoMock();

  WidgetController.hitTestWarningShouldBeFatal = true;

  group('AddOrEditFAB', () {
    testWidgets('Add button', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      MigraineEditorPathParameters? params;
      var routeBuilder = (text) {
        return (context) {
          params = ModalRoute.of(context)!.settings.arguments
              as MigraineEditorPathParameters?;
          return Text(text);
        };
      };
      var navO = MockNavigatorObserver();
      var c = MockTabController();
      var cfg = MigraineLogConfig();
      var l = MigraineList(config: cfg);
      var s = MigraineLogGlobalState();
      when(() => c.index).thenReturn(0);
      await tester.pumpWidget(materialWidget(Container(),
          fab: AddOrEditFAB(
            list: l,
            state: s,
            controller: c,
          ),
          navObserver: navO,
          routes: {
            '/editor': routeBuilder('/editor'),
          }));
      verify(() => navO.didPush(any(), any()));
      expect(find.text("/editor"), findsNothing);
      expect(find.byIcon(Icons.add), findsOneWidget);
      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();
      //verify(navO.didPush(any, any));
      var captured = verify(() => navO.didPush(captureAny(), any())).captured;
      Route newRoute = captured[0] as Route;
      expect(newRoute.settings.name, '/editor');
      expect(find.text("/editor"), findsOneWidget);
      expect(params!.entry!.isNewEntry, isTrue);
    });
    group('Multi button', () {
      testWidgets('add', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        MigraineEditorPathParameters? params;
        var routeBuilder = (text) {
          return (context) {
            params = ModalRoute.of(context)!.settings.arguments
                as MigraineEditorPathParameters?;
            return Text(text);
          };
        };
        var currDate = DateTime(2021, 1, 1);
        var navO = MockNavigatorObserver();
        var c = MockTabController();
        var cfg = MigraineLogConfig();
        var l = MigraineList(config: cfg);
        var e = MigraineEntry();
        e.date = currDate;
        e.strength = MigraineStrength.headache;
        l.set(e);
        var s = MigraineLogGlobalState();
        s.currentDate = currDate;
        when(() => c.index).thenReturn(1);
        await tester.pumpWidget(materialWidget(Container(),
            fab: AddOrEditFAB(
              list: l,
              state: s,
              controller: c,
            ),
            navObserver: navO,
            routes: {
              '/editor': routeBuilder('/editor'),
            }));
        expect(params, isNull);
        expect(find.byIcon(Icons.add), findsNothing);
        expect(find.text("/editor"), findsNothing);
        expect(find.byWidgetPredicate((w) => w is FloatingActionButton),
            findsOneWidget);
        await tester.tap(
            find.byWidgetPredicate((w) => w is FloatingActionButton).first);
        await tester.pumpAndSettle();
        expect(find.byIcon(Icons.add), findsOneWidget);
        expect(find.byKey(const Key("speedDialAddEntry")), findsOneWidget);
        await tester.tap(find.byKey(const Key("speedDialAddEntry")));
        await tester.pumpAndSettle();
        expect(find.text("/editor"), findsOneWidget);
        expect(params, TypeMatcher<MigraineEditorPathParameters>());
        expect(params!.entry!.date, currDate);

        await tester.flush();
      });
      testWidgets('edit', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        MigraineEditorPathParameters? params;
        var routeBuilder = (text) {
          return (context) {
            params = ModalRoute.of(context)!.settings.arguments
                as MigraineEditorPathParameters?;
            return Text(text);
          };
        };
        var currDate = DateTime(2021, 1, 1);
        var navO = MockNavigatorObserver();
        var c = MockTabController();
        var cfg = MigraineLogConfig();
        var l = MigraineList(config: cfg);
        var e = MigraineEntry();
        e.date = currDate;
        e.strength = MigraineStrength.headache;
        l.set(e);
        var s = MigraineLogGlobalState();
        s.currentDate = currDate;
        when(() => c.index).thenReturn(1);
        await tester.pumpWidget(materialWidget(Container(),
            fab: AddOrEditFAB(
              list: l,
              state: s,
              controller: c,
            ),
            navObserver: navO,
            routes: {
              '/editor': routeBuilder('/editor'),
            }));
        expect(params, isNull);
        expect(find.byIcon(Icons.edit), findsNothing);
        expect(find.text("/editor"), findsNothing);
        await tester.tap(
            find.byWidgetPredicate((w) => w is FloatingActionButton).first);
        await tester.pumpAndSettle();
        expect(find.byIcon(Icons.edit), findsOneWidget);
        expect(find.byKey(const Key("speedDialEditEntry")), findsOneWidget);
        await tester.tap(find.byKey(const Key("speedDialEditEntry")));
        await tester.pumpAndSettle();
        expect(find.text("/editor"), findsOneWidget);
        expect(params, TypeMatcher<MigraineEditorPathParameters>());
        expect(params!.entry!.isClone, isTrue);

        await tester.flush();
      });
      testWidgets('delete', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        MigraineEditorPathParameters? params;
        var routeBuilder = (text) {
          return (context) {
            params = ModalRoute.of(context)!.settings.arguments
                as MigraineEditorPathParameters?;
            return Text(text);
          };
        };
        var currDate = DateTime(2021, 1, 1);
        var navO = MockNavigatorObserver();
        var c = MockTabController();
        var cfg = MigraineLogConfig();
        var l = MigraineList(config: cfg);
        var e = MigraineEntry();
        e.date = currDate;
        e.strength = MigraineStrength.headache;
        l.set(e);
        var s = MigraineLogGlobalState();
        s.currentDate = currDate;
        when(() => c.index).thenReturn(1);
        await tester.pumpWidget(materialWidget(Container(),
            fab: AddOrEditFAB(
              list: l,
              state: s,
              controller: c,
            ),
            navObserver: navO,
            routes: {
              '/editor': routeBuilder('/editor'),
            }));
        expect(params, isNull);
        expect(find.byIcon(Icons.delete), findsNothing);
        expect(find.byType(SnackBar), findsNothing);
        // Open the FAB
        await tester.tap(
            find.byWidgetPredicate((w) => w is FloatingActionButton).first);
        await tester.pumpAndSettle();
        expect(l.exists(currDate), isTrue);
        // Tap on delete
        expect(find.byIcon(Icons.delete), findsOneWidget);
        expect(find.byKey(const Key("speedDialDeleteEntry")), findsOneWidget);
        await tester.tap(find.byKey(const Key("speedDialDeleteEntry")));
        await tester.pumpAndSettle();
        expect(l.exists(currDate), isFalse, reason: 'Should have been deleted');
        // Undo via the SnackBar
        expect(find.byType(SnackBar), findsOneWidget);
        await tester.tap(find.byType(SnackBarAction));
        await tester.pumpAndSettle();
        expect(l.exists(currDate), isTrue,
            reason: "Action shuold have been undone");

        await tester.flush();
      });
    });
  });
  testWidgets('MigraineLogAppLifecycle', (WidgetTester tester) async {
    var c = MockConfig();
    var l = MockList();
    await tester.pumpWidget(materialWidget(MigraineLogAppLifecycle(
      list: l,
      config: c,
      child: Text('MigraineLogAppLifecycle-child'),
    )));
    when(() => l.saveData()).thenAnswer((_) async {
      return true;
    });
    when(() => c.saveConfig()).thenAnswer((_) async {
      return true;
    });
    verifyNever(() => c.saveConfig());
    verifyNever(() => l.saveData());
    tester.binding.handleAppLifecycleStateChanged(AppLifecycleState.paused);
    verify(() => c.saveConfig());
    verify(() => l.saveData());
    tester.binding.handleAppLifecycleStateChanged(AppLifecycleState.inactive);
    verify(() => c.saveConfig());
    verify(() => l.saveData());
    tester.binding.handleAppLifecycleStateChanged(AppLifecycleState.detached);
    verify(() => c.saveConfig());
    verify(() => l.saveData());
    tester.binding.handleAppLifecycleStateChanged(AppLifecycleState.resumed);
    verifyNever(() => c.saveConfig());
    verifyNever(() => l.saveData());

    expect(find.text('MigraineLogAppLifecycle-child'), findsOneWidget);
  });
  group('MigraineLogHome', () {
    testWidgets('golden stock', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      MigraineList l = MigraineList(config: c);
      var pi = MockPackageInfo();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogHome(packageInfo: pi),
        provide: [
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
          ChangeNotifierProvider.value(value: c),
        ],
      ));

      await goldenTest(
        widgetType: MigraineLogHome,
        name: 'MigraineLogHome.stock',
        tester: tester,
      );
    }, skip: skipGoldenTests);
    testWidgets('golden content', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      var now = DateTime.now();
      MigraineList l = MigraineList(config: c);
      var pi = MockPackageInfo();
      var e = MigraineEntry(parentList: l);
      e.date = now;
      e.strength = MigraineStrength.migraine;
      l.set(e);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogHome(packageInfo: pi),
        provide: [
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
          ChangeNotifierProvider.value(value: c),
        ],
      ));

      await goldenTest(
        widgetType: MigraineLogHome,
        name: 'MigraineLogHome.content',
        tester: tester,
      );

      await tester.flush();
    }, skip: skipGoldenTests);
    testWidgets('Switch to calendar', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var navO = MockNavigatorObserver();
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      var pi = MockPackageInfo();
      when(() => pi.version).thenReturn("TEST");
      var now = DateTime.now();
      MigraineList l = MigraineList(config: c);
      var e = MigraineEntry(parentList: l);
      e.date = now;
      e.strength = MigraineStrength.migraine;
      l.set(e);
      await tester.pumpWidget(
        materialWidgetBuilder(
          (context) => MigraineLogHome(packageInfo: pi),
          provide: [
            ChangeNotifierProvider.value(value: l),
            ChangeNotifierProvider.value(value: s),
            ChangeNotifierProvider.value(value: c),
          ],
          navObserver: navO,
        ),
      );
      expect(find.byType(MigraineLogViewer), findsNothing);
      expect(find.byIcon(Icons.calendar_today), findsOneWidget);
      await tester.tap(find.byIcon(Icons.calendar_today));
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogViewer), findsOneWidget);
      await tester.flush();
    });
    testWidgets('Switch to stats', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var navO = MockNavigatorObserver();
      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      var pi = MockPackageInfo();
      when(() => pi.version).thenReturn("TEST");
      var now = DateTime.now();
      MigraineList l = MigraineList(config: c);
      var e = MigraineEntry(parentList: l);
      e.date = now;
      e.strength = MigraineStrength.migraine;
      l.set(e);
      await tester.pumpWidget(
        materialWidgetBuilder(
          (context) => MigraineLogHome(packageInfo: pi),
          provide: [
            ChangeNotifierProvider.value(value: l),
            ChangeNotifierProvider.value(value: s),
            ChangeNotifierProvider.value(value: c),
          ],
          navObserver: navO,
        ),
      );
      expect(find.byType(MigraineLogStatsViewer), findsNothing);
      expect(find.byIcon(Icons.pie_chart), findsOneWidget);
      await tester.tap(find.byIcon(Icons.pie_chart));
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogStatsViewer), findsOneWidget);
      await tester.flush();
    });
    group('menu', () {
      testWidgets('about', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        var navO = MockNavigatorObserver();
        var c = MigraineLogConfig();
        var s = MigraineLogGlobalState();
        var pi = MockPackageInfo();
        when(() => pi.version).thenReturn("TEST");
        var now = DateTime.now();
        MigraineList l = MigraineList(config: c);
        var e = MigraineEntry(parentList: l);
        e.date = now;
        e.strength = MigraineStrength.migraine;
        l.set(e);
        await tester.pumpWidget(
          materialWidgetBuilder(
            (context) => MigraineLogHome(packageInfo: pi),
            provide: [
              ChangeNotifierProvider.value(value: l),
              ChangeNotifierProvider.value(value: s),
              ChangeNotifierProvider.value(value: c),
            ],
            navObserver: navO,
          ),
        );
        verify(() => navO.didPush(any(), any()));
        expect(find.byIcon(Icons.more_vert), findsOneWidget);
        expect(find.text('About'), findsNothing);

        await tester.tap(find.byIcon(Icons.more_vert));
        await tester.pumpAndSettle();

        expect(find.text('About'), findsOneWidget);
        await tester.tap(find.text('About'));
        await tester.pumpAndSettle();

        verify(() => navO.didPush(any(), any()));

        expect(find.byType(AboutDialog), findsOneWidget);

        await tester.flush();
      });
      testWidgets('help', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        var routeBuilder = (text) {
          return (context) {
            return Text(text);
          };
        };
        var navO = MockNavigatorObserver();
        var c = MigraineLogConfig();
        var s = MigraineLogGlobalState();
        var pi = MockPackageInfo();
        when(() => pi.version).thenReturn("TEST");
        var now = DateTime.now();
        MigraineList l = MigraineList(config: c);
        var e = MigraineEntry(parentList: l);
        e.date = now;
        e.strength = MigraineStrength.migraine;
        l.set(e);
        await tester.pumpWidget(
          materialWidgetBuilder((context) => MigraineLogHome(packageInfo: pi),
              provide: [
                ChangeNotifierProvider.value(value: l),
                ChangeNotifierProvider.value(value: s),
                ChangeNotifierProvider.value(value: c),
              ],
              navObserver: navO,
              routes: {
                '/help': routeBuilder('/help'),
              }),
        );
        verify(() => navO.didPush(any(), any()));
        expect(find.byIcon(Icons.more_vert), findsOneWidget);
        expect(find.text('Help'), findsNothing);

        await tester.tap(find.byIcon(Icons.more_vert));
        await tester.pumpAndSettle();

        expect(find.text('Help'), findsOneWidget);
        await tester.tap(find.text('Help'));
        await tester.pumpAndSettle();

        expect(find.text("/help"), findsOneWidget);

        await tester.flush();
      });
      testWidgets('debug', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        var navO = MockNavigatorObserver();
        var c = MigraineLogConfig();
        var s = MigraineLogGlobalState();
        var pi = MockPackageInfo();
        when(() => pi.version).thenReturn("TEST");
        MigraineList l = MigraineList(config: c);
        await tester.pumpWidget(
          materialWidgetBuilder(
            (context) => MigraineLogHome(packageInfo: pi),
            provide: [
              ChangeNotifierProvider.value(value: l),
              ChangeNotifierProvider.value(value: s),
              ChangeNotifierProvider.value(value: c),
            ],
            navObserver: navO,
          ),
        );
        verify(() => navO.didPush(any(), any()));
        expect(l.isNotEmpty, isFalse);
        expect(find.byIcon(Icons.more_vert), findsOneWidget);
        expect(find.text('Build debug entries'), findsNothing);

        await tester.tap(find.byIcon(Icons.more_vert));
        await tester.pumpAndSettle();

        expect(find.text('Build debug entries'), findsOneWidget);
        await tester.tap(find.text('Build debug entries'));
        await tester.pumpAndSettle();

        expect(l.isNotEmpty, isTrue);

        await tester.flush();
      });
      // TODO
      testWidgets('export', (WidgetTester tester) async {}, skip: true);
      testWidgets('import', (WidgetTester tester) async {}, skip: true);
      // TODO
      testWidgets('exit', (WidgetTester tester) async {}, skip: true);
      testWidgets('settings', (WidgetTester tester) async {
        await initializeDateFormatting("en", null);
        var routeBuilder = (text) {
          return (context) {
            return Text(text);
          };
        };
        var navO = MockNavigatorObserver();
        var c = MigraineLogConfig();
        var s = MigraineLogGlobalState();
        var pi = MockPackageInfo();
        when(() => pi.version).thenReturn("TEST");
        var now = DateTime.now();
        MigraineList l = MigraineList(config: c);
        var e = MigraineEntry(parentList: l);
        e.date = now;
        e.strength = MigraineStrength.migraine;
        l.set(e);
        await tester.pumpWidget(
          materialWidgetBuilder((context) => MigraineLogHome(packageInfo: pi),
              provide: [
                ChangeNotifierProvider.value(value: l),
                ChangeNotifierProvider.value(value: s),
                ChangeNotifierProvider.value(value: c),
              ],
              navObserver: navO,
              routes: {
                '/config': routeBuilder('/config'),
              }),
        );
        // FIXME
        //verify(() => navO.didPush(any(), any()));
        expect(find.byIcon(Icons.more_vert), findsOneWidget);
        expect(find.text('Settings'), findsNothing);

        await tester.tap(find.byIcon(Icons.more_vert));
        await tester.pumpAndSettle();

        expect(find.text('Settings'), findsOneWidget);
        await tester.tap(find.text('Settings'));
        await tester.pumpAndSettle();

        expect(find.text("/config"), findsOneWidget);

        await tester.flush();
      });
    });
  });
  group('MigraineLogLoader', () {
    testWidgets('Loading', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var navO = MockNavigatorObserver();
      var c = MockConfig();
      var s = MigraineLogGlobalState();
      var pi = MockPackageInfo();
      when(() => pi.version).thenReturn("TEST");
      // Assume onboarding done
      when(() => c.onboardingVersion).thenReturn(9999);
      var now = DateTime.now();
      MigraineList l = MockList();
      when(() => l.loadData()).thenAnswer((_) async {
        return FileLoadStatus.success;
      });
      when(() => l.headacheDaysStats(any()))
          .thenAnswer((_) => MigraineLogStatisticsList(ofDays: 30));
      when(() => l.entries).thenReturn(0);
      when(() => c.loadConfig()).thenAnswer((_) async {
        return;
      });
      var e = MigraineEntry(parentList: l);
      e.date = now;
      e.strength = MigraineStrength.migraine;
      l.set(e);
      await tester.pumpWidget(
        materialWidgetBuilder(
            (context) => SizedBox(
                  height: 700,
                  width: 400,
                  child: MigraineLogLoader(
                      list: l, title: "Hello world", config: c, state: s),
                ),
            provide: [
              ChangeNotifierProvider.value(value: l),
              ChangeNotifierProvider.value(value: s),
              ChangeNotifierProvider.value(value: c),
            ],
            navObserver: navO,
            routes: {}),
      );
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      await tester.pump(Duration(seconds: 20));
      expect(find.byType(CircularProgressIndicator), findsNothing);
      await tester.flush();
    });
  });
}

class MockTabController extends Mock implements TabController {}

class MockPackageInfo extends Mock implements PackageInfo {}

class MockPathProviderPlatform extends Mock
    with MockPlatformInterfaceMixin
    implements PathProviderPlatform {
  MockPathProviderPlatform({this.dir});
  Directory? dir;

  @override
  Future<String> getApplicationDocumentsPath() async {
    return dir!.path;
  }
}
