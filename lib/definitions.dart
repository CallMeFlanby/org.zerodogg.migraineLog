// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2025   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// The default date format.
/// Note: updates to this should also be reflected in formattedDateTime in web.dart
const String DateRenderFormat = 'EEEE, d. MMMM yyyy';

/// The link to the website
final Uri WebsiteURL = Uri.https('migrainelog.zerodogg.org', '/');

/// The link to the GPL
final Uri GPL_URL = Uri.https("www.gnu.org", "/licenses/gpl-3.0.html");

/// The current data version
const DATAVERSION = 4;

/// How strong a migraine attack is
enum MigraineStrength {
  strongMigraine,
  migraine,
  headache,
  auraOnly,
  noHeadache
}

/// This is an extension on our MigraineStrength enum, allowing for conversions
/// to and from a predefined numeric value.
///
/// To get a numeric value use .numeric on a MigraineStrength value (ie.
/// MigraineStrength.migraine.numeric). To convert a numeric value back to a
/// MigraineStrength, use val = MigraineStrengthConverter.fromNumber(integer);
extension MigraineStrengthConverter on MigraineStrength {
  // Yes, having these two maps are a bit ugly, but it gives us the advantages
  // of enums (compile time checks in switch statements for example) while also
  // providing consistent values for cold storage and a way to display the
  // numeric values to the user (in exported data).
  static const Map _strengthToInt = {
    MigraineStrength.strongMigraine: 3,
    MigraineStrength.migraine: 2,
    MigraineStrength.headache: 1,
    MigraineStrength.auraOnly: -1,
    MigraineStrength.noHeadache: -2,
  };
  static const Map _strengthFromInt = {
    3: MigraineStrength.strongMigraine,
    2: MigraineStrength.migraine,
    1: MigraineStrength.headache,
    -1: MigraineStrength.auraOnly,
    -2: MigraineStrength.noHeadache,
  };

  /// Convert this MigraineStrength to its numeric value
  int get humanNumeric => _strengthToInt[this];

  /// Convert a numeric migraine strength into its MigraineStrength value
  static MigraineStrength? fromNumber(int no) => _strengthFromInt[no];
}
