# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

require 'yaml'
require 'pp'

default_platform(:android)

platform :android do
  desc "Publish a new beta"
  lane :beta do
    upload_to_play_store(track: 'beta', aab: '../build/app/outputs/bundle/release/app-release.aab')
  end
  desc "Publish a new production"
  lane :production do
    upload_to_play_store(track: 'production', aab: '../build/app/outputs/bundle/release/app-release.aab')
  end
  desc "Extract release changelog"
  lane :changelog do
      system("mkdir","-p","./metadata/android/en-GB/changelogs","./metadata/android/no-NO/changelogs")
      meta = YAML.safe_load(File.read("../../pubspec.yaml"))
      version = meta["version"]
      changelog = read_changelog(
          section_identifier: "["+version+"]",
          changelog_path: "../ChangeLog.md",
      )
      if changelog !~ /\S/
          throw("Missing changelog");
      end
      File.write("metadata/android/en-GB/changelogs/default.txt",changelog)
      changelog = read_changelog(
          section_identifier: "["+version+"]",
          changelog_path: "../ChangeLog.no.md",
      )
      if changelog !~ /\S/
          throw("Missing changelog");
      end
      File.write("metadata/android/no-NO/changelogs/default.txt",changelog)
  end
  desc "Extract beta changelog"
  lane :betaChangelog do
      system("mkdir","-p","./metadata/android/en-GB/changelogs","./metadata/android/no-NO/changelogs")
      meta = YAML.safe_load(File.read("../../pubspec.yaml"))
      version = meta["version"]
      changelog = read_changelog(
          changelog_path: "../ChangeLog.md",
          section_identifier: "[Unreleased]",
      )
      if changelog !~ /\S/
          changelog = read_changelog(
              section_identifier: "["+version+"]",
              changelog_path: "../ChangeLog.md",
          )
      end
      File.write("metadata/android/en-GB/changelogs/default.txt",changelog)
      changelog = read_changelog(
          changelog_path: "../ChangeLog.no.md",
          section_identifier: "[Unreleased]",
      )
      if changelog !~ /\S/
          changelog = read_changelog(
              section_identifier: "["+version+"]",
              changelog_path: "../ChangeLog.no.md",
          )
      end
      File.write("metadata/android/no-NO/changelogs/default.txt",changelog)
  end
end
